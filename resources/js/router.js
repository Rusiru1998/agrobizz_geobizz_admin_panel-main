import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

import APILogin from "./components/APILogin.vue";
import ApiRegistration from "./components/APIRegistration.vue";
import DashboardView from "./components/AdminPanel/adminDashboard.vue";
import GroupsView from "./components/AdminPanel/pageViewGroups.vue";
import pageEditGroupView from "./components/AdminPanel/pageEditGroup.vue";
import MemberView from "./components/AdminPanel/pageViewMembers.vue";
import pageEditMemberView from "./components/AdminPanel/pageEditMember.vue";
import pageViewRecordsView from "./components/AdminPanel/pageViewRecords.vue";
import AdminSettingsView from "./components/AdminPanel/AdminSettings.vue";
import TransferOfOwnershipView from "./components/AdminPanel/TransferOfOwnership.vue";
import MailAccountsView from "./components/AdminPanel/mailAccounts.vue";
import sendEmailView from "./components/AdminPanel/sendEmail.vue";
import ImportCSVView from "./components/AdminPanel/ImportCSV.vue";
import BackupsView from "./components/AdminPanel/Backups.vue";
import FileBackupsView from "./components/AdminPanel/FileBackups.vue";
import FullBackupsView from "./components/AdminPanel/FullBackups.vue";
import DatabaseBackupsView from "./components/AdminPanel/DatabaseBackups.vue";
import backforEmailsView from "./components/AdminPanel/backforEmails.vue";
import restoreView from "./components/AdminPanel/restore.vue";
import errorsView from "./components/AdminPanel/errors.vue";
import IPBlockerView from "./components/AdminPanel/IPBlocker.vue";
import manageAPITokensView from "./components/AdminPanel/manageAPITokens.vue";
import noticeManagementView from "./components/AdminPanel/noticeManagement.vue";
import userLoginManagementView from "./components/AdminPanel/userLoginManagement.vue";
import siteInformationView from "./components/AdminPanel/siteInformation.vue";
import allUserInformation from "./components/AdminPanel/allUserInformationView.vue";

const routes = [{
        path: "/",
        component: APILogin
    },
    {
        path: "/apiRegistration",
        name: "apiRegistration",
        component: ApiRegistration
    },
    {
        path: "/AdminPanel/admin",
        name: "admin",
        component: DashboardView
    },
    {
        path: "/AdminPanel/pageViewGroups",
        name: "pageViewGroups",
        component: GroupsView
    },
    {
        path: "/AdminPanel/pageEditGroup",
        name: "pageEditGroup",
        component: pageEditGroupView
    },
    {
        path: "/AdminPanel/pageViewMembers",
        name: "pageViewMembers",
        component: MemberView
    },
    {
        path: "/AdminPanel/pageEditMember",
        name: "pageEditMember",
        component: pageEditMemberView
    },
    {
        path: "/AdminPanel/pageViewRecords",
        name: "pageViewRecords",
        component: pageViewRecordsView
    },
    {
        path: "/AdminPanel/AdminSettings",
        name: "AdminSettings",
        component: AdminSettingsView
    },
    {
        path: "/AdminPanel/TransferOfOwnership",
        name: "TransferOfOwnership",
        component: TransferOfOwnershipView
    },
    {
        path: "/AdminPanel/mailAccounts",
        name: "mailAccounts",
        component: MailAccountsView
    },
    {
        path: "/AdminPanel/sendEmail",
        name: "sendEmail",
        component: sendEmailView
    },
    {
        path: "/AdminPanel/ImportCSV",
        name: "ImportCSV",
        component: ImportCSVView
    },
    {
        path: "/AdminPanel/Backups",
        name: "Backups",
        component: BackupsView
    },
    {
        path: "/AdminPanel/FileBackups",
        name: "FileBackups",
        component: FileBackupsView
    },
    {
        path: "/AdminPanel/FullBackups",
        name: "FullBackups",
        component: FullBackupsView
    },
    {
        path: "/AdminPanel/DatabaseBackups",
        name: "DatabaseBackups",
        component: DatabaseBackupsView
    },
    {
        path: "/AdminPanel/backforEmails",
        name: "backforEmails",
        component: backforEmailsView
    },
    {
        path: "/AdminPanel/errors",
        name: "errors",
        component: errorsView
    },
    {
        path: "/AdminPanel/restore",
        name: "restore",
        component: restoreView
    },
    {
        path: "/AdminPanel/IPBlocker.vue",
        name: "IPBlocker",
        component: IPBlockerView
    },
    {
        path: "/AdminPanel/manageAPITokens.vue",
        name: "manageAPITokens",
        component: manageAPITokensView
    },
    {
        path: "/AdminPanel/noticeManagement",
        name: "noticeManagement",
        component: noticeManagementView
    },
    {
        path: "/AdminPanel/userLoginManagement.vue",
        name: "userLoginManagement",
        component: userLoginManagementView
    },
    {
        path: "/AdminPanel/siteInformation",
        name: "siteInformation",
        component: siteInformationView
    },
    {
        path: "/AdminPanel/allUserInformationView:userId",
        name: "allUserInformation",
        component: allUserInformation
    },
];

export default new Router({
    mode: "history",
    routes
});
