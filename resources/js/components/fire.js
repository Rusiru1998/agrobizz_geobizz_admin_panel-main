import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCLibvCRI7GdzAabP-Of1guabu_CZLXpg8",
    authDomain: "agrobizz.firebaseapp.com",
    databaseURL: "https://agrobizz-default-rtdb.firebaseio.com",
    projectId: "agrobizz",
    storageBucket: "agrobizz.appspot.com",
    messagingSenderId: "659711777399",
    appId: "1:659711777399:web:fde58adae6a3a9c4580472",
    measurementId: "G-YXF9Z5E6HM"
};
// Initialize Firebase
var fire = firebase.initializeApp(firebaseConfig);
firebase.analytics();
firebase.database();
export default fire;
