import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    gmap:null,
    gmapSecond:null,
    patients:null,
    patientStats:{},
    visiblePatients:null,
    httpheader:{},
    geocode:{},
    clusters:[],
    pageload:true,
    selectedLocation:null,
    currentGNDData:null,
    statBar:false,
    searchBar:false,
    weatherYear:'2019',
    weatherDistrict:'colombo',
    farmers:[],
    all_farms:[],
    mapQuery:[],
    fertlizerAnalysis:[],
    allCrops:[],
    proximityAnalysis:[],
    proximityBuffer:null,
    visible_farmers:[],
    visible_farms:[],
    all_agents:[],
      mapContextMenu:{
          x:0,
          y:0,
          visible:false,
          tasks:[]
      },
     contextMenuAction:{
        type:null,
        drawer:null,
        drawerNo:null,
        tabID:null,
        tabNo:null
     },
      gisCategories:null,
      gisLayers:null,
      excelLayer:null,
      myInfoWindow:null,
      mapLayerStyle:{},
      viewMapStyleWindow:false,
      customPointLayer:[],
      agentTrackingPoint:null,
      ongoingTab:null,
      visibleSecondaryMapWindow:false,
      mapToolDrawingType:null,
      clusterLocations:[],
      adminBoundaryType:null,
      analyseFilterSet:[],
      allHarvesting:[],
      tradingAnalysisDateStart:null,
      tradingAnalysisDateEnd:null,
      tradingAnalysisDateRange:null,
      tradingAnalysisData:[],
  },
  mutations: {
    changeMap(state, gmap) {
      state.gmap = gmap;
    },
    setHttpHeader(state, payload) {
      state.httpheader = payload;
    },
    setPatients(state,payload){
      state.patients = payload;
    },
    setVisiblePatients(state,payload){
      state.visiblePatients = payload;
    },
    pushToPatients(state,payload){
      state.patients.push(payload);
    },

    setMap(state,payload){
      state.gmap = payload;
    },
    setPageLoad(state,loading){
      state.pageload = loading;
    },
    setSearchBar(state, status) {
      state.searchBar = status;
    },
    setGeocode(state,payload){
      state.geocode = payload;
    },
    setSelectedLocation(state,payload){
      state.selectedLocation = payload;
    },
    setcurrentGNDData(state,payload){
      state.currentGNDData = payload;
    },
    setStatBar(state, status) {
      state.statBar = status;
    },
    setWeekDengueLoader(state,status){
      console.log(status);
      state.weekDengueLoader = status;
    },
    setWeatherYear(state,status){
      state.weatherYear = status;
    },
    setFarmers(state,payload){
      state.farmers = payload;
    },
    setPatientStats(state,status){
     state.patientStats = status;
    },
    setFertilizerAnalysis(state,payload){
     state.fertlizerAnalysis = payload;
    },
    setProximityAnalysis(state,payload){
     state.proximityAnalysis = payload;
    },
    setAllCrops(state,payload){
     state.allCrops = payload;
    },
    setProximityBuffer(state,payload){
     state.proximityBuffer = payload;
    },
    setVisibleFarmers(state,payload){
     state.visible_farmers = payload;
    },
    setVisibleFarms(state,payload){
     state.visible_farms = payload;
    },
    setAllFarms(state,payload){
     state.all_farms = payload;
    },
    showMapContextMenu(state,payload){
        state.mapContextMenu.x = payload.datax;
        state.mapContextMenu.y = payload.datay;
        state.mapContextMenu.latLng = payload.latLng;
        state.mapContextMenu.address = payload.address;
        state.mapContextMenu.visible = true;
    },

    addToMapContextMenu(state , payload){
        let menuAlreadyExists = _.find(state.mapContextMenu.tasks,{'id':payload.id});
        if (!menuAlreadyExists)
            state.mapContextMenu.tasks.push(payload);
    } ,

    removeFromMapContextMenu(state , payload){
        let menuIndex = _.findIndex(state.mapContextMenu.tasks,{'id':payload.id});
        if (menuIndex >= 0){
            state.mapContextMenu.tasks.splice(menuIndex,1);
        }
    },

    setContextMenuAction(state , payload){
        state.contextMenuAction = payload;
    },

    resetContextMenuAction(state , reset){
        if (reset)
            state.contextMenuAction = {type:null, drawer:null, drawerNo:null, tabID:null, tabNo:null};
    },
    setGISCategories(state,payload){
        state.gisCategories = payload;
    },
    setGISLayers(state,payload){
        state.gisLayers = payload;
    },
    setExcelLayers(state,payload){
        state.excelLayer = payload;
    },
    setMyInfoWindow(state,payload){
        state.myInfoWindow = payload;
    },
    setMapLayerStyle(state,payload){
        state.mapLayerStyle = payload;
    },
    setMapStyleWindow(state,view){
        state.viewMapStyleWindow = view;
    },
    setCustomPointLayer(state,payload){
        state.customPointLayer = payload;
    },
    setAgents(state,payload){
        state.all_agents = payload;
    },
    setAgentTrackingPoint(state,payload){
        state.agentTrackingPoint = payload;
    },
    setOngoingTab(state,tabID){
        state.ongoingTab = tabID;
    },
    removeOngoingTab(state,tabID){
        state.ongoingTab = null;
    },
    setSecondMap(state,payload){
        state.gmapSecond = payload;
    },
    setVisibilityofSecondaryMap(state,visibility){
        state.visibleSecondaryMapWindow = visibility;
    },
    setMapToolDrawingType(state,type){
        state.mapToolDrawingType = type;
    },
    setClusterLocations(state,payload){
        state.clusterLocations = payload;
    },
    pushToClusterLocations(state,payload){
      state.clusterLocations.push(payload);
    },
    setAdminBoundaryType(state,payload){
      state.adminBoundaryType = payload;
    },
    setAnalyseFilterSet(state,payload){
      state.analyseFilterSet = payload;
    },
    setAllHarvesting(state,payload){
      state.allHarvesting = payload;
    },
    setTradingAnalyseDate(state,payload){
      state.tradingAnalysisDateStart = payload.start;
      state.tradingAnalysisDateEnd = payload.end;
      state.tradingAnalysisDateRange = payload.range;
    },
    setTradingAnalyseDataset(state,payload){
      state.tradingAnalysisDateStart = payload.start;
    },
    tradingAnalysisData(state,payload){
      state.tradingAnalysisData = payload;
    },


  },
  getters: {
    patients: state => state.patients,
    visiblePatients: state => state.visiblePatients,
    clusters: state => state.clusters,
    gmap: state => state.gmap,
    pageload: state => state.pageload,
    selectedLocation: state => state.selectedLocation,
    currentGNDData: state => state.currentGNDData,
    geocode: state => state.geocode,
    searchBar: state => state.searchBar,
    statBar: state => state.statBar,
    weatherYear: state => state.weatherYear,
    farmers: state => state.farmers,
    patientStats: state => state.patientStats,
    fertlizerAnalysis: state => state.fertlizerAnalysis,
    allCrops: state => state.allCrops,
    proximityAnalysis: state => state.proximityAnalysis,
    proximityBuffer: state => state.proximityBuffer,
    visible_farmers: state => state.visible_farmers,
    visible_farms: state => state.visible_farms,
    all_farms: state => state.all_farms,
    mapContextMenu: state => state.mapContextMenu,
    contextMenuAction: state => state.contextMenuAction,
    gisCategories: state => state.gisCategories,
    gisLayers: state => state.gisLayers,
    excelLayer: state => state.excelLayer,
    mapLayerStyle: state => state.mapLayerStyle,
    viewMapStyleWindow: state => state.viewMapStyleWindow,
    customPointLayer: state => state.customPointLayer,
    all_agents: state => state.all_agents,
    agentTrackingPoint: state => state.agentTrackingPoint,
    ongoingTab: state => state.ongoingTab,
    gmapSecond: state => state.gmapSecond,
    visibleSecondaryMapWindow: state => state.visibleSecondaryMapWindow,
    mapToolDrawingType: state => state.mapToolDrawingType,
    clusterLocations: state => state.clusterLocations,
    adminBoundaryType: state => state.clusterLocations,
    analyseFilterSet: state => state.analyseFilterSet,
    allHarvesting: state => state.allHarvesting,
    tradingAnalysisDateStart: state => state.tradingAnalysisDateStart,
    tradingAnalysisDateEnd: state => state.tradingAnalysisDateEnd,
    tradingAnalysisDateRange: state => state.tradingAnalysisDateRange,
    tradingAnalysisData: state => state.tradingAnalysisData,

  }
});
