<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="pane scroller">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>ADMIN PANEL</title>
        <link href="{{asset('/css/app.css') }}" rel="stylesheet">
        <script>
            window.Laravel = '{!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => Auth::user(),
        ]) !!}';
        </script>
        <style>
            .pane {
                color: #2b2b2b;
                height: 16.1em;
                overflow-y: scroll;
                border: none !important;
            }
            .scroller {
                scrollbar-color: #2b2b2b;
                scrollbar-width: thin;
            }
            .scroller::-webkit-scrollbar {
                width: 10px;
                border: none !important;
            }
            .scroller::-webkit-scrollbar-track {
                background: #9E9E9E;
                border: none !important;
            }
            .scroller::-webkit-scrollbar-thumb {
                background: #2b2b2b;
                border: none !important;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <mainapp></mainapp>
        </div>
    </body>
    <script src="{{asset('/js/app.js')}}"></script>
</html>
