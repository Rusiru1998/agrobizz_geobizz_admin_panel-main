<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="pane scroller">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ADMIN PANEL</title>
        <link href="{{asset('/css/app.css') }}" rel="stylesheet">
        <style>
            a[href^="http://maps.google.com/maps"]{display:none !important}
            a[href^="https://maps.google.com/maps"]{display:none !important}
            .gmnoprint a, .gmnoprint span, .gm-style-cc {
                display:none;
            }
            html{
                /* overflow: hidden !important; */
                width: 100%;
                height: 100%;
                font-size: 0.8em !important;
            }
            label{
                font-size: 0.8em !important;
            }


        </style>
        <script>
            window.Laravel = '{!! json_encode([
                    'csrfToken' => csrf_token(),
                    'user' => \App\User::with('locations')->where('id','=',Auth::id())->first(),
                ]) !!}';
            window.setLogout = function () {
                document.getElementById('logout-form').submit();
            };
        </script>
    </head>
    <body>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <v-app id="app">
            <router-view></router-view>
        </v-app>
    </body>
    <script src="{{asset('/js/app.js')}}"></script>
</html>
