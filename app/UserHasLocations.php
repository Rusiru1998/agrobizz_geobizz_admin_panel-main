<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHasLocations extends Model
{
    use SoftDeletes;
    protected $table = 'user_has_locations';
}
