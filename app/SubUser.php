<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubUser extends Model
{
    protected $table = 'sub_users';
}
