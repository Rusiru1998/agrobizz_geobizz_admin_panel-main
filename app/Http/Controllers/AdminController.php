<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use stdClass;

class AdminController extends Controller {
    public function adminHome(){
        return view('admin.adminHome');
    }
}
