<?php

namespace App\Http\Controllers;


use App\Agent;
use App\AgentTrackingPoint;
use App\Company;
use App\Crop;
use App\CropCategory;
use App\CropDamage;
use App\CustomLayers;
use App\DailyCollection;
use App\Devices;
use App\Events\notifyFarmer;
use App\Events\notifyRegistration;
use App\Farm;
use App\Farmer;
use App\Fertilizer;
use App\GISCategories;
use App\GNDOriginalDetails;
use App\MapDrawingLayers;
use App\MarkerDataset;
use App\PublicPlaces;
use App\User;
use App\UserHasLocations;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class APIController extends Controller
{

    //----------------------------Login DETAILS-------------------------------------------------------------------------
    public function farmerRegister(Request $request){
        $this->createAPILogFile($request->post(), 'farmerRegister');
        Log::info($request);
        $created_by = $request->input('uid');
        $farmer_id = $request->input('farmer_Id');
        $name = $request->input('Farmer_Name');
        $address = $request->input('address');
        $google_address = $request->input('google_address');
        $postal_Code = $request->input('postal_Code');
        $gender = $request->input('gender');
        $dob = $request->input('dob');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $nic = $request->input('nic');
        $mobile = $request->input('mobile');
        $email = $request->input('email');
        $password = $request->input('password');
        $education = $request->input('education');
        $image = $request->input('image');
        $no_of_earners = $request->input('no_Of_Earning');
        $no_of_adults = $request->input('no_Of_Adults');
        $no_of_childrens = $request->input('no_Of_Children');





        $company_name = $request->input('company_Name');
        $group_name = $request->input('group_Name');


        $agent = User::where('id','=',$created_by)->first();

        if (!$agent){
            return response()->json(['http_status' => 'error','message'=>'invalid agent detail.check your agent_email'],200);
        }
        if (!$email){
            $email = $name.'@gmail.com';
        }

        $company = $request->input('company');

        $source_of_income = $request->input('income');
        $nok_name = $request->input('nok_Name');
        $nok_mobile = $request->input('nok_mobile');

        //-----------------creating new user------------------------
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->mobile = $mobile;
        $user->role = "farmer";

        $password?$user->password = $password:$user->password = "Abcdef@123";

        if ($user->save()) {
            $gndData = DB::select("SELECT gndName , province,district,dsd FROM tbl_sl_gnd_data WHERE ST_CONTAINS(f_polygon, Point('{$lat}' ,'{$lng}')) ");

            $farmer = new Farmer();
            $farmer->gender = $gender;
            $farmer->dob = $dob;
            $farmer->name = $name;
            $farmer->email = $email;

            $farmer->address = $address;
            $farmer->google_address = $google_address;
            $farmer->nic = $nic;


            $farmer->group_name = $group_name;
            $farmer->company_name = $company_name;
            $farmer->lat = $lat;
            $farmer->lng = $lng;
            $farmer->no_of_earners = $no_of_earners;
            $farmer->no_of_adults = $no_of_adults;
            $farmer->no_of_childrens = $no_of_childrens;
            $farmer->education = $education;
            $farmer->user_id = $user->id;
            $farmer->created_by = $agent->id;
            $farmer->postal_Code = $postal_Code;
            $farmer->farmer_id = 'AG'.$agent->id.'_F'.$user->id;
            $farmer->source_of_income = $source_of_income;
            $farmer->nok_name = $nok_name;
            $farmer->nok_mobile = $nok_mobile;
            $farmer->mobile = $mobile;


            if ($gndData && count($gndData) > 0) {
                $farmer->gnd = $gndData[0]->gndName;
                $farmer->dsd = $gndData[0]->dsd;
                $farmer->district = $gndData[0]->district;
                $farmer->province = $gndData[0]->province;
            }
            if($image){
                $image = 'data:image/jpeg;base64,' . $image;
                if (preg_match('/^data:image\/(\w+);base64,/', $image)) {
                    $ext = explode(';base64', $image);
                    $ext = explode('/', $ext[0]);
                    $ext = $ext[1];

                    $data = substr($image, strpos($image, ',') + 1);
                    $data = base64_decode($data);
                    $name = 'FRMR'.$user->id.'_AG'.$agent->id . '.' . $ext;
                    $url = "img/farmer/" . $name;
                    Storage::put($url, $data);
                    $farmer->image = 'https://agrobizz.net/storage/'.$url;
                }
            }
            if($farmer->save()){
                event(new notifyFarmer($farmer));
                return response()->json(['http_status' => 'success','message'=>'farmer_registered_successfully','farmer'=>$farmer],200);
            }



        }
    }
    //-----------------------------------------return farmers list-------------------------------------------
    public function getFarmers(Request $request){
        $farmers = Farmer::with('farms')->orderBy('id','DESC')->get();
        $crops = Crop::with('farmer','farm')->get();
        $farms = Farm::orderBy('id','DESC')->get();
        $gis_categories = GISCategories::orderBY('main_category','ASC')->get();
        $agents = User::with(['devices','farmers'])->where('role','=','agent')->get();
        return response()->json(['http_status' => 'success','farmers'=>$farmers,'crops'=>$crops,'farms'=>$farms,'gis_categories'=>$gis_categories,'agents'=>$agents],200);
    }
    //-----------------------------------------return farmer by ID-------------------------------------------
    public function getFarmerByID(Request $request){
        $farmer_id = $request->input('farmer_id');
        $farmer = Farmer::where('farmer_id','=',$farmer_id)->first();
        Log::info($request);
        if(!$farmer){
            return response()->json(['http_status' => 'error','message'=>'No farmers Available for  '.$farmer_id],200);
        }else{
            return response()->json(['http_status' => 'success','data'=>$farmer],200);
        }


    }

    //--------------------------------------create farm----------------------------------------------------------------
    public function farmRegister(Request $request){

        $this->createAPILogFile($request->post(), 'farmRegister');
        Log::info($request);

        $image = $request->input('image');
        $gases   =  $request->input('gases');
        $velocity   =  $request->input('velocity');
        $radiation   =  $request->input('radiation');
        $humidity   =  $request->input('humidity');
        $temperature   =  $request->input('temperature');
        $annual_rain   =  $request->input('annual_rain');
        $animal_risk   =  $request->input('animal_risk');
        $drought_disk   =  $request->input('drought_disk');
        $flood_risk   =  $request->input('flood_risk');
        $typeof_Quality_Water   =  $request->input('typeof_Quality_Water');
        $typeofFarm_mashers   =  $request->input('typeofFarm_mashers');
        $typeof_Disease_Control   =  $request->input('typeof_Disease_Control');
        $typeof_Irrigation   =  $request->input('typeof_Irrigation');
        $typeof_Water_Source   =  $request->input('typeof_Water_Source');
        $typeof_System   =  $request->input('typeof_System');
        $type_Of_Legal_status   =  $request->input('type_Of_Legal_status');
        $type_of_Farming   =  $request->input('type_of_Farming');
        $land_Protection   =  $request->input('land_Protection');
        $landType   =  $request->input('landType');
        $Payments   =  $request->input('Payments');
        $Working_Hours   =  $request->input('Working_Hours');
        $Sources_Female   =  $request->input('Sources_Female');
        $Sources_Male   =  $request->input('Sources_Male');
        $Quantity_For_Day    =  $request->input('Quantity_For_Day');
        $Soil_Reactions    =  $request->input('Soil_Reactions');
        $Soil_Organisms    =  $request->input('Soil_Organisms');
        $Soil_Organic_Matter    =  $request->input('Soil_Organic_Matter');
        $Soil_Mineral    =  $request->input('Soil_Mineral');
        $Soil_Temperature    =  $request->input('Soil_Temperature');
        $Soil_Air    =  $request->input('Soil_Air');
        $soil_moisture    =  $request->input('soil_moisture');
        $lng    =  $request->input('lng');
        $lat    =  $request->input('lat');
        $address    =  $request->input('address');
        $google_address    =  $request->input('google_address');
        $land_size    =  $request->input('land_size');
        $farm_name    =  $request->input('farm_name');
        $farmer_Id    =  $request->input('farmer_Id');
        $created_by    =  $request->input('uid');
        $farm_id    =  $request->input('farm_id');
        $type    =  $request->input('type');
        $farm = null;

        $agent = User::where('id','=',$created_by)->first();
        $farmer  = Farmer::where('farmer_id','=',$farmer_Id)->first();
        $type = 'inserted';
        if (!$agent){
            return response()->json(['http_status' => 'error','message'=>'invalid agent detail.check your agent_email'],200);
        }

        if (!$farmer){
            return response()->json(['http_status' => 'error','message'=>'invalid farmer detail.check your farmer_id'],200);
        }

        if ($farm_id){
            $existing_farm = Farm::where('farm_id','=',$farm_id)->first();
            if (!$existing_farm){
                return response()->json(['http_status' => 'error','message'=>'invalid farm ID.if you need insert as new one. ignore the farm_id attribute'],200);
            }else{
                $type = 'updated';
                $farm = Farm::find($existing_farm->id);
            }
        }else {
            $farm = new Farm();
            $max = DB::table('farms')->max('id');;
            $max += 1;
            $farm_id = 'AG'.$agent->id.'_F'.$farmer->id.'_FRM'.$max;
        }
            $farm->gases = $gases;
            $farm->velocity = $velocity;
            $farm->radiation  = $radiation;
            $farm->humidity  = $humidity;
            $farm->temperature   = $temperature;
            $farm->annual_rain  = $annual_rain;
            $farm->animal_risk  = $animal_risk;
            $farm->drought_disk  = $drought_disk;
            $farm->flood_risk  = $flood_risk;
            $farm->typeof_Quality_Water  = $typeof_Quality_Water;
            $farm->typeofFarm_mashers   = $typeofFarm_mashers;
            $farm->typeof_Disease_Control  = $typeof_Disease_Control;
            $farm->typeof_Irrigation = $typeof_Irrigation;
            $farm->typeof_Water_Source  = $typeof_Water_Source;
            $farm->typeof_System  = $typeof_System;
            $farm->type_Of_Legal_status = $type_Of_Legal_status;
            $farm->type_of_Farming = $type_of_Farming;
            $farm->land_Protection  = $land_Protection;
            $farm->landType  = $landType;
            $farm->Payments  = $Payments;
            $farm->Working_Hours  = $Working_Hours;
            $farm->Sources_Female  = $Sources_Female;
            $farm->Sources_Male = $Sources_Male;
            $farm->Quantity_For_Day  = $Quantity_For_Day;
            $farm->Soil_Reactions = $Soil_Reactions;
            $farm->Soil_Organisms  = $Soil_Organisms;
            $farm->Soil_Organic_Matter = $Soil_Organic_Matter;
            $farm->Soil_Mineral  = $Soil_Mineral;
            $farm->Soil_Temperature = $Soil_Temperature;
            $farm->Soil_Air  = $Soil_Air;
            $farm->soil_moisture  = $soil_moisture;
            $farm->lng  = $lng;
            $farm->lat  = $lat;
            $farm->address  = $address;
            $farm->farm_name = $farm_name;
            $farm->farmer_Id  = $farmer_Id;
            $farm->created_by  = $agent->id;
            $farm->farm_id  = $farm_id;


            $gndData = DB::select("SELECT gndName , province,district,dsd FROM tbl_sl_gnd_data WHERE ST_CONTAINS(f_polygon, Point('{$lat}' ,'{$lng}')) ");
            if ($gndData && count($gndData) > 0) {
                $farm->gnd = $gndData[0]->gndName;
                $farm->dsd = $gndData[0]->dsd;
                $farm->district = $gndData[0]->district;
                $farm->province = $gndData[0]->province;
            }

        if($image){
            $image = 'data:image/jpeg;base64,' . $image;
            if (preg_match('/^data:image\/(\w+);base64,/', $image)) {
                $ext = explode(';base64', $image);
                $ext = explode('/', $ext[0]);
                $ext = $ext[1];

                $data = substr($image, strpos($image, ',') + 1);
                $data = base64_decode($data);
                $name = $farm_id . '.' . $ext;
                $url = "img/farm/" . $name;
                Storage::put($url, $data);
                $farm->image = 'https://agrobizz.net/storage/'.$url;
            }
        }

        if ($farm->save()){

            return response()->json(['http_status' => 'success','message'=>'farm '.$type.' successfully' ,'farm_id '=>$farm_id],200);
        }else{
            return response()->json(['http_status' => 'error','message'=>'farm cannot be saved'],200);
        }






    }
    //----------------------------------------create update crop----------------------------------------------------
    public function addCrop(Request $request){
        $created_by = $request->input('uid');
        $farmer_Id = $request->input('farmer_id');
        $farm_id = $request->input('farm_id');
        $crop_type = $request->input('crop_type');
        $main_crop = $request->input('main_crop');
        $major_crop = $request->input('major_crop');
        $sub_crop = $request->input('sub_crop');
        $cultivated_land_area = $request->input('cultivated_land_area');
        $number_of_crops = $request->input('number_of_crops');
        $plant_date = $request->input('plant_date');
        $havested_date = $request->input('havested_date');
        $crop_area = $request->input('crop_area');
        $expected_price = $request->input('expected_price');
        $expected_quantity = $request->input('expected_quantity');
        $previous_crops = $request->input('previous_crops');
        $crop_id = $request->input('crop_id');

        $agent = User::where('id','=',$created_by)->first();
        $farmer  = Farmer::where('farmer_id','=',$farmer_Id)->first();

        $crop = null;
        if (!$agent){
            return response()->json(['http_status' => 'error','message'=>'invalid agent detail.check your agent_email'],200);
        }

        if (!$farmer){
            return response()->json(['http_status' => 'error','message'=>'invalid farmer detail.check your farmer_id'],200);
        }
        $crop = new Crop();

        $crop->previous_crops = $previous_crops;
        $crop->expected_quantity = $expected_quantity;
        $crop->expected_price = $expected_price;
        $crop->expected_price = $expected_price;
        $crop->crop_area = $crop_area;
        $crop->havested_date = $havested_date;
        $crop->plant_date = $plant_date;
        $crop->number_of_crops = $number_of_crops;
        $crop->cultivated_land_area = $cultivated_land_area;
        $crop->sub_crop = $sub_crop;
        $crop->major_crop = $major_crop;
        $crop->crop_type = $crop_type;
        $crop->created_by = $agent->id;
        $crop->main_crop = $main_crop;
        $crop->farmer_id = $farmer_Id;
        $crop->farm_id = $farm_id;

        if ($crop->save()){
            $crop_id? $type = 'updated':$type = 'saved';
            return response()->json(['http_status' => 'success','message'=>'crop '.$type.' successfully' ,'crop'=>$crop],200);
        }else{
            return response()->json(['http_status' => 'error','message'=>'crop cannot be saved'],200);
        }

    }
    //----------------------------------------create update bank details----------------------------------------------------
    public function bankDetails(Request $request){
        $farmer_Id = $request->input('farmer_Id');
        $account_name = $request->input('account_name');
        $bank_name = $request->input('bank_name');
        $branch_name = $request->input('branch_name');
        $account_number = $request->input('account_number');
        $account_id = $request->input('account_id');
        $agent_email = $request->input('agent_email');

        $agent = User::where('email','=',$agent_email)->first();
        $farmer  = Farmer::where('farmer_id','=',$farmer_Id)->first();

        $bank = null;
        if (!$agent){
            return response()->json(['http_status' => 'error','message'=>'invalid agent detail.check your agent_email'],200);
        }

        if (!$farmer){
            return response()->json(['http_status' => 'error','message'=>'invalid farmer detail.check your farmer_id'],200);
        }
        if ($account_id){
            $getBankDetails = BankDetails::where('id','=',$account_id)->first();
            if (!$getBankDetails)
                return response()->json(['http_status' => 'error','message'=>'invalid Bank ID.if you need insert as new one. ignore the account_id attribute'],200);
            else
                $bank = BankDetails::find($getBankDetails->id);
        }else{
            $bank = new BankDetails();
        }

        $bank->farmer_Id = $farmer_Id;
        $bank->account_number = $account_number;
        $bank->branch_name = $branch_name;
        $bank->bank_name = $bank_name;
        $bank->account_name = $account_name;
        $bank->created_by = $agent->id;


        if ($bank->save()){
            $account_id? $type = 'updated':$type = 'saved';
            return response()->json(['http_status' => 'success','message'=>'bank account '.$type.' successfully' ,'account_id '=>$bank->id],200);
        }else{
            return response()->json(['http_status' => 'error','message'=>'bank cannot be saved'],200);
        }

    }
    //----------------------------------------create update fertilizer----------------------------------------------------
    public function fertilizer(Request $request){
        $farmer_Id = $request->input('farmer_id');
        $qty_fertilizer = $request->input('qty_fertilizer');
        $period_fertilizer = $request->input('period_fertilizer');
        $type_fertilizer = $request->input('type_fertilizer');
        $fertilizer_id = $request->input('fertilizer_id');
        $created_by = $request->input('uid');

        $farmer  = Farmer::where('farmer_id','=',$farmer_Id)->first();
        $fertilizer = null;

        if (!$farmer){
            return response()->json(['http_status' => 'error','message'=>'invalid farmer detail.check your farmer_id'],200);
        }
        if ($fertilizer_id){
            $getFertilizer = Fertilizer::where('id','=',$fertilizer_id)->first();
            if (!$getFertilizer)
                return response()->json(['http_status' => 'error','message'=>'invalid Fertilizer ID.if you need insert as new one. ignore the fertilizer_id attribute'],200);
            else
                $fertilizer = Fertilizer::find($getFertilizer->id);
        }else{
            $fertilizer = new Fertilizer();
        }

        $fertilizer->qty_fertilizer = $qty_fertilizer;
        $fertilizer->period_fertilizer = $period_fertilizer;
        $fertilizer->type_fertilizer = $type_fertilizer;
        $fertilizer->created_by = $created_by;
        $fertilizer->farmer_id = $farmer_Id;



        if ($fertilizer->save()){
            $fertilizer_id? $type = 'updated':$type = 'saved';
            return response()->json(['http_status' => 'success','message'=>'Fertilizer '.$type.' successfully' ,'account_id '=>$fertilizer->id],200);
        }else{
            return response()->json(['http_status' => 'error','message'=>'Fertilizer cannot be saved'],200);
        }

    }
    //----------------------------------------create update processProducts----------------------------------------------------
    public function processProducts(Request $request){
        $farmer_Id = $request->input('farmer_Id');
        $product_name = $request->input('product_name');
        $type_product = $request->input('type_product');
        $ingredients = $request->input('ingredients');
        $Container = $request->input('Container');
        $qty = $request->input('qty');
        $pp_id = $request->input('process_products_id');
        $agent_email = $request->input('agent_email');

        $agent = User::where('email','=',$agent_email)->first();
        $farmer  = Farmer::where('farmer_id','=',$farmer_Id)->first();

        $processProducts = null;
        if (!$agent){
            return response()->json(['http_status' => 'error','message'=>'invalid agent detail.check your agent_email'],200);
        }

        if (!$farmer){
            return response()->json(['http_status' => 'error','message'=>'invalid farmer detail.check your farmer_id'],200);
        }
        if ($pp_id){
            $getProcessProducts = ProcessProducts::where('id','=',$pp_id)->first();
            if (!$getProcessProducts)
                return response()->json(['http_status' => 'error','message'=>'invalid Process Product ID.if you need insert as new one. ignore the process_products_id attribute'],200);
            else
                $processProducts = ProcessProducts::find($getProcessProducts->id);
        }else{
            $processProducts = new ProcessProducts();
        }

        $processProducts->product_name = $product_name;
        $processProducts->type_product = $type_product;
        $processProducts->ingredients = $ingredients;
        $processProducts->Container = $Container;
        $processProducts->qty = $qty;
        $processProducts->created_by = $agent->id;
        $processProducts->farmer_id = $farmer_Id;



        if ($processProducts->save()){
            $pp_id? $type = 'updated':$type = 'saved';
            return response()->json(['http_status' => 'success','message'=>'Process Products '.$type.' successfully' ,'account_id '=>$processProducts->id],200);
        }else{
            return response()->json(['http_status' => 'error','message'=>'Process Products cannot be saved'],200);
        }

    }
    //-------------------------------------------create log file for the API CALL---------------------------------------
    public function createAPILogFile($request,$method){
        $today = Carbon::now()->format('y_m_d');
        $text_name = $method.'_' . time() . '.txt';
        $url = 'API_CALL/'.$today.'/'.$text_name;
        $data = $request;
        Storage::put($url, $data);
    }
    //---------------------------Location Main DAtatset------------------------------------------------
    public function getLocationDataFromFarmer(Request $request){
        $type = $request->input('type');
        $value = $request->input('value');
        $data = null;
        $farms = [];
        $farmers = [];
        switch ($type){
            case 'province': $value == 'All'? $data = Farmer::whereNotNull('district')->where('district','!=',"")->groupBy('district')->pluck('district')->toArray(): $data = GNDOriginalDetails::where('province','=',$value)->whereNotNull('district')->where('district','!=',"")->groupBy('district')->pluck('district')->toArray();
                $farms = Farm::where('province','=',$value)->groupBy('id')->pluck('id')->toArray();
                $farmers = Farmer::with('farms')->where('province','=',$value)->get();
                break;
            case 'district': $data = Farmer::where('district','=',$value)->whereNotNull('dsd')->where('dsd','!=',"")->groupBy('dsd')->pluck('dsd')->toArray();
                $farms = Farm::where('district','=',$value)->groupBy('id')->pluck('id')->toArray();
                $farmers = Farmer::with('farms')->where('district','=',$value)->get();
                break;
            case 'dsd': $data = Farmer::where('dsd','=',$value)->whereNotNull('gnd')->where('gnd','!=',"")->groupBy('gnd')->pluck('gnd')->toArray();
                $farms = Farm::with('farms')->where('dsd','=',$value)->groupBy('id')->pluck('id')->toArray();
                $farmers = Farmer::where('dsd','=',$value)->get();
                break;
            case 'gnd': $data = [];
                $farms = Farm::where('gnd','=',$value)->groupBy('id')->pluck('id')->toArray();
                $farmers = Farmer::with('farms')->where('gnd','=',$value)->get();
                break;
        }
        $main_crops = Crop::whereIn('farm_id',$farms)->groupBy('main_crop')->pluck('main_crop')->toArray();
        $all_crops = Crop::whereIn('farm_id',$farms)->get();

        return response()->json(['http_status' => 'success', 'data' => $data,'main_crops'=>$main_crops , 'all_crops'=>$all_crops ,'farms'=>$farms,'farmers'=>$farmers], 200);
    }
    //---------------------------Location for Fertilizer------------------------------------------------
    public function getLocationDataFromFertilizer(Request $request){
        $type = $request->input('type');
        $value = $request->input('value');
        $data = null;
        $farmIDS = [];
        $farmers = [];
        $fertilizerFarmers = Fertilizer::groupBy('farmer_id')->pluck('farmer_id')->toArray();
        switch ($type){
            case 'province': $value == 'All'? $data = Farmer::whereNotNull('district')->where('district','!=',"")->whereIn('farmer_id',$fertilizerFarmers)->groupBy('district')->pluck('district')->toArray(): $data = GNDOriginalDetails::where('province','=',$value)->whereNotNull('district')->where('district','!=',"")->groupBy('district')->pluck('district')->toArray();
                $farmIDS = Farmer::where('province','=',$value)->groupBy('farmer_id')->pluck('farmer_id')->toArray();
                $farmers = Farmer::with('farms')->where('province','=',$value)->get();
                break;
            case 'district': $data = Farmer::where('district','=',$value)->whereNotNull('dsd')->where('dsd','!=',"")->whereIn('farmer_id',$fertilizerFarmers)->groupBy('dsd')->pluck('dsd')->toArray();
                 $farmIDS = Farmer::where('district','=',$value)->groupBy('farmer_id')->pluck('farmer_id')->toArray();
                $farmers = Farmer::with('farms')->where('district','=',$value)->get();
                break;
            case 'dsd': $data = Farmer::where('dsd','=',$value)->whereNotNull('gnd')->where('gnd','!=',"")->whereIn('farmer_id',$fertilizerFarmers)->groupBy('gnd')->pluck('gnd')->toArray();
                 $farmIDS = Farmer::where('dsd','=',$value)->groupBy('farmer_id')->pluck('farmer_id')->toArray();
                $farmers = Farmer::where('dsd','=',$value)->get();
                break;
            case 'gnd': $data = [];
                 $farmIDS = Farmer::where('gnd','=',$value)->groupBy('farmer_id')->pluck('farmer_id')->toArray();
                $farmers = Farmer::with('farms')->where('gnd','=',$value)->get();
                break;
        }
        $fertilizers = Fertilizer::whereIn('farmer_id',$farmIDS)->whereNotNull('type_fertilizer')->select('type_fertilizer',DB::raw('count(*) as farmers'))->groupBy('type_fertilizer')->get();
        $all_fertilizer = Fertilizer::whereIn('farmer_id',$farmIDS)->get();

        return response()->json(['http_status' => 'success', 'data' => $data,'fertilizers'=>$fertilizers , 'all_fertilizer'=>$all_fertilizer ,'farmers'=>$farmers], 200);
    }
    //---------------------------Location Main Datatset------------------------------------------------
    public function getLocationDataFromBoundary(Request $request){
        $type = $request->input('type');
        $value = $request->input('value');
        $data = null;
        switch ($type){
            case 'province': $value == 'All'? $data = GNDOriginalDetails::whereNotNull('district')->where('district','!=',"")->groupBy('district')->pluck('district')->toArray(): $data = GNDOriginalDetails::where('province','=',$value)->whereNotNull('district')->where('district','!=',"")->groupBy('district')->pluck('district')->toArray();
                break;
            case 'district': $data = GNDOriginalDetails::where('district','=',$value)->whereNotNull('dsd')->where('dsd','!=',"")->groupBy('dsd')->pluck('dsd')->toArray();
                break;
            case 'dsd': $data = GNDOriginalDetails::where('dsd','=',$value)->whereNotNull('gndName')->where('gndName','!=',"")->groupBy('gndName')->pluck('gndName')->toArray();
                break;
        }

        return response()->json(['http_status' => 'success', 'data' => $data], 200);
    }
    //----------------------get proxmity Analysis----------------------------------------------------------
    public function proximityAnalysis(Request $request){
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $radius = $request->input('radius');
        $mainCrop = $request->input('main_crop');
        $subCrop = $request->input('sub_crop');

        $farmers = collect();
        $get_farmers = Farmer::getByDistance($lat,$lng,$radius);
        foreach ($get_farmers as $farmer){
            if ($mainCrop && $mainCrop !== 'All'){
                if ($subCrop && $subCrop !== 'All'){
                    $crop = Crop::where('farmer_id','=',$farmer->farmer_id)->where('sub_crop','=',$subCrop)->first();
                    if ($crop){
                        $farmers->push($farmer);
                    }
                }else{
                    $crop = Crop::where('farmer_id','=',$farmer->farmer_id)->where('main_crop','=',$mainCrop)->first();
                    if ($crop){
                        $farmers->push($farmer);
                    }
                }
            }else{
                $farmers->push($farmer);
            }
        }
        return response()->json(['http_status' => 'success', 'data'=>$farmers ]);
    }
    //-------------------------------------------weather API call---------------------------------------------
    public function weatherAPI(Request $request){
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $key = '2a175a084d58ddcbda647a9c153172c2';
        $client = new Client();
        try {
            $response = $client->request('GET', 'https://api.openweathermap.org/data/2.5/weather?lat=' . $lat . '&lon=' .$lng. '&appid='.$key.'', [
                'verify' => false,
            ]);
            $response->getBody()->rewind();
            $response = json_decode($response->getBody()->getContents());
//                                return response()->json(['http_status' => 'error', 'response' => $response->results[0]->geometry]);
            return response()->json(['http_status'=>'success','response'=>$response]);
        } catch (GuzzleException $e) {
            return response()->json(['http_status'=>'error','message'=>$e]);
        }
    }
    //------------------------------------------load custom images-----------------------------------------------------
    public function getCustomLayers(){
        $customOverlays = CustomLayers::all();
        return response()->json(['http_status'=>'success','layers'=>$customOverlays]);
    }
    //------------------------------------------create custom image-----------------------------------------------------
    public function uploadCustomImage(Request $request){
      $bottom_lat   = $request->input('bottom_lat');
      $bottom_lng   = $request->input('bottom_lng');
      $top_lat   = $request->input('top_lat');
      $top_lng   = $request->input('top_lng');
      $name   = $request->input('name');
      $created_by   = $request->input('uid');
      $image = $request->file('image');

      $customOverlay = new CustomLayers();
      $customOverlay->name = $name;
      $customOverlay->top_lat = $top_lat;
      $customOverlay->top_lng = $top_lng;
      $customOverlay->bottom_lat = $bottom_lat;
      $customOverlay->bottom_lng = $bottom_lng;
      $customOverlay->created_by = $created_by;

        if ($image) {
            $upload = Storage::putFile('img/customLayers', $image);
            $customOverlay->url = 'storage/'.$upload;
        }
        if ($customOverlay->save()){
            return response()->json(['http_status'=>'success','layer'=>$customOverlay,'message'=>'data upload successfully']);
        }else{
            return response()->json(['http_status'=>'error','message'=>'cannot insert new layer']);
        }
    }
    //--------------------------------------------=create daily collection dataset--------------------------------------
    public function createDailyCollection(Request $request){
        $created_by    =  $request->input('uid');
        $farmer_id    =  $request->input('farmer_id');
        $main_crop    =  $request->input('main_crop');
        $sub_crop    =  $request->input('sub_crop');
        $ttl_harvesting    =  $request->input('ttl_harvesting');
        $quantity    =  $request->input('quantity');
        $purchased_price    =  $request->input('purchased_price');
        $collection_date    =  $request->input('collection_date');

        $dailyCollection = new DailyCollection();
        $dailyCollection->farmer_id = $farmer_id;
        $dailyCollection->main_crop = $main_crop;
        $dailyCollection->sub_crop = $sub_crop;
        $dailyCollection->ttl_harvesting = $ttl_harvesting;
        $dailyCollection->quantity = $quantity;
        $dailyCollection->purchased_price = $purchased_price;
        $dailyCollection->collection_date = $collection_date;
        $dailyCollection->created_by = $created_by;
        if ($dailyCollection->save()){
            return response()->json(['http_status'=>'success','daily_collection'=>$dailyCollection,'message'=>'added to daily collection']);
        }

    }
    //----------------------------Add new tracking device---------------------------------------------------------------
    public function agentRegister(Request $request){
        $created_by    =  $request->input('uid');
        $name    =  $request->input('name');
        $category    =  $request->input('category');
        $mobile    =  $request->input('mobile');
        $email    =  $request->input('email');
        $imei    =  $request->input('imei');
        $nickname    =  $request->input('nickname');
        $address    =  $request->input('address');

        $existUSer = User::where('email','=',$email)->first();

        $existUSer?$agent = User::find($existUSer->id):$agent = new User();
        $agent->email = $email;
        $agent->name = $name;
        $agent->password = Hash::make('Abcdef@123');
        $agent->role = "agent";
        $agent->status = "active";
        $agent->save();
        $devices_exist = Devices::where('imei','=',$imei)->first();
        if ($devices_exist){
            return response()->json(['http_status' => 'error','message'=>'imei already exists'],200);
        }

        $device = new  Devices();
        $device->created_by = $created_by;
        $device->name = $name;
        $device->category  = $category;
        $device->mobile  = $mobile;
        $device->email  = $email;
        $device->imei   = $imei;
        $device->nickname  = $nickname;
        $device->address  = $address;
        $device->user_id  = $agent->id;
        if($device->save()){
            return response()->json(['http_status' => 'success','message'=>'agent registered','device'=>$device],200);
        }

    }
    //-------------------------------------Add Selected District to table-----------------------------------------------
    public function addSelectedDistrict(Request $request){
        $user_id = $request->input('uid');
        $locations = $request->input('locations');
        $locations = json_decode($locations);

        $deleteIfExists = UserHasLocations::where('user_id','=',$user_id)->delete();

        foreach ($locations as $location) {
            $ulocation = new UserHasLocations();
            $ulocation->user_id = $user_id;
            $ulocation->location = $location;
            $ulocation->save();
        }
        return response()->json(['http_status'=>'success']);
    }
    //-----------------------------get map icon url set-----------------------------------------------------------------
    public function getMapIcons(){
        $url = 'img/icons';
        $markers =  Storage::allFiles($url);
        return response()->json(['message' => 'success', 'markers' =>$markers ]);
    }
    //-----------------------------save custom marker set-----------------------------------------------------------------
    public function saveCustomMarkers(Request $request){
        $marker_array = $request->input('marker_array');
        $dataset = $request->input('dataset');
        $created_by = $request->input('uid');

        $marker_array = json_decode($marker_array);
        foreach ($marker_array as $marker) {
            $customMarkers = new MarkerDataset();
            $customMarkers->srno = $marker->SRNo;
            $customMarkers->name = $marker->name;
            $customMarkers->details = $marker->detail;

            if (property_exists($marker,'iconURL'))
                $customMarkers->icon = $marker->iconURL;

            $customMarkers->lat = $marker->lat;
            $customMarkers->lng = $marker->lng;
            if (property_exists($marker,'color'))
                $customMarkers->color = $marker->color;
            if (property_exists($marker,'size'))
                $customMarkers->size = $marker->size;
            $customMarkers->dataset = $dataset;
            $customMarkers->created_by = $created_by;
            $customMarkers->save();
        }

        $markers = MarkerDataset::all();
        $dataset_list = MarkerDataset::groupBy('dataset')->pluck('dataset')->toArray();

        return response()->json(['http_status'=>'success','markerSet'=>$markers,'dataset_list'=>$dataset_list,'message'=>'marker set saved successfully']);
    }
    //-----------------------------get custom marker set-----------------------------------------------------------------
    public function getCustomMarkers(){
        $markers = MarkerDataset::all();
        $dataset_list = MarkerDataset::groupBy('dataset')->pluck('dataset')->toArray();
        return response()->json(['http_status'=>'success','markerSet'=>$markers,'message'=>'marker set saved successfully','dataset_list'=>$dataset_list]);
    }
    //-----------------------------save geojson dataset----------------------------------------------------------------
    public function saveGeoJson(Request $request){
        $created_by = $request->input('uid');
        $layerName = $request->input('layername');
        $layerType = $request->input('layerType');
        $jsonData = $request->input('jsonData');
        $filepath = 'mapLayers/'.$layerName.'.json';

        Storage::put($filepath,$jsonData);


        $mapLayers = new MapDrawingLayers();
        $mapLayers->name = $layerName;
        $mapLayers->layer_type = $layerType;
        $mapLayers->created_by = $created_by;
        $mapLayers->url = '../storage/'.$filepath;


        if ($mapLayers->save()){
            $layerSet = MapDrawingLayers::all();
            $dataset_list = MapDrawingLayers::groupBy('name')->pluck('name')->toArray();
            return response()->json(['http_status'=>'success','layerSet'=>$layerSet,'message'=>'layer saved successfully','layer_list'=>$dataset_list]);
        }

    }
    //-----------------------------get saved geojson layer set-----------------------------------------------------------------
    public function getGeoJsonLayers(Request $request){
        $type = $request->input('type');
        $layerSet = MapDrawingLayers::all();
        $dataset_list = MapDrawingLayers::where('layer_type','=',$type)->groupBy('name')->pluck('name')->toArray();
        return response()->json(['http_status'=>'success','layerSet'=>$layerSet,'layer_list'=>$dataset_list]);
    }
    //----------------------------------get agent last location-------------------------------------------------------
    public function getAgentLastLocation(Request $request){
        $agentEmail = $request->input('email');
        $agent = User::where('email','=',$agentEmail)->first();
        $location = AgentTrackingPoint::where('user_id','=',$agent->id)->orderBy('id','DESC')->first();
        return response()->json(['http_status'=>'success','location'=>$location]);
    }
    //------------------------------------------Hide user App hint------------------------------------------------------
    public function hideAppHint(Request $request){
        $uid = $request->input('uid');
        $getUser = User::where('id','=',$uid)->first();
        if ($getUser){
            $user = User::find($getUser->id);
            $user->showHint = 'false';
            if($user->save()){
                return response()->json(['http_status'=>'success']);
            }
        }
    }

    //------------------------------------------Get Crop Damages------------------------------------------------------
    public function getCropDamages(Request $request){
        $cropDamages = CropDamage::with('farmer')->get();
        return response()->json(['http_status'=>'success','data'=>$cropDamages]);
    }

    //------------------------------------------Get Crop Damages------------------------------------------------------
    public function getAllCrops(Request $request){
        $cropDamages = CropCategory::all();
        return response()->json(['http_status'=>'success','data'=>$cropDamages]);
    }
    //------------------------------------------Get Agents------------------------------------------------------
    public function getAgents(Request $request){
        $data = User::with(['devices','farmers','agentPoints'])->where('role','=','agent')->get();
        return response()->json(['http_status'=>'success','data'=>$data]);
    }

    public function getGNDData(Request $request){
        $type = $request->input('type');
        $value = $request->input('value');

        $data = [];
        switch ($type){
            case 'province' : $data =  GNDOriginalDetails::groupBy('province')->pluck('province')->toArray();
                              break;
            case 'district' : $data =  GNDOriginalDetails::where('province','=',$value)->groupBy('district')->pluck('district')->toArray();
                              break;
            case 'dsd'      : $data =  GNDOriginalDetails::whereIn('district',$value)->groupBy('dsd')->pluck('dsd')->toArray();
                              break;
            case 'gnd'      : $data =  GNDOriginalDetails::whereIn('dsd',$value)->groupBy('gndName')->pluck('gndName')->toArray();
                              break;
        }
        return response()->json(['http_status'=>'success','data'=>$data]);
    }


}
