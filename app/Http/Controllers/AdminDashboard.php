<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\SubUser;
use App\permisson;
use App\User;

class AdminDashboard extends Controller
{
    public function getWebUsers()
    {
        $data = permisson::sum('web');
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }

    public function getMobileUsers()
    {
        $data = permisson::sum('mobile');
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }

    public function getBlockUsers()
    {
        $data = User::where('status', 'block')->count();
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }

    public function getAdmin()
    {
        $data = User::where('role', 'admin')->count();
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }
    public function getActiveUsers()
    {
        $data = User::where('status', 'active')->get();
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }

    public function getBlackListUsers()
    {
        $data = User::where('status', 'block')->get();
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($data, 200);
    }

    public function addUsers(Request $request)
    {
        $request->validate(
            [
                'fname' => 'required',
                'lname' => 'required',
                'nic' => 'required|max:10',
                'email' => 'required',
                'address' => 'required',
                'dob' => 'required',
                'mobile' => 'required|max:10',
                'img' => 'required',
            ]
        );

        $brand_image = $request->file('img');

        $name_gen = hexdec(uniqid());
        $img_extention = strtolower($brand_image->getClientOriginalExtension());
        $img_name = $name_gen . '.' . $img_extention;
        $upload_location = 'image/users/';
        $image = $upload_location . $img_name;
        $brand_image->move($upload_location, $img_name);

        $user = User::find($request->uid);
        if (!$user)

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->address = $request->address;
        $user->mobile = $request->mobile;
        $user->save();

        $user = new SubUser();

        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->nic = $request->nic;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->dob = $request->dob;
        $user->mobile = $request->mobile;
        $user->image = $image;
        $user->users_id = $request->uid;



        // $data = DB::table('sub_users')->insert(
        //     array(
        //         'fname' => $request->fname,
        //         'lname' => $request->lname,
        //         'nic' => $request->nic,
        //         'email' => $request->email,
        //         'address' => $request->address,
        //         'dob' => $request->dob,
        //         'mobile' => $request->mobile,
        //         'image' => $image,
        //         'users_id' => Auth::user()->id,
        //     )
        // );

        if ($user->save())
            return response()->json(['message' => 'Successfully Added User'], 200);
        else
            return response()->json(['message' => 'Failed to Create'], 500);
    }

    public function allUsers()
    {
        $data = SubUser::all();
        // $data = DB::table('sub_users')->get();
        if (is_null($data)) return response()->json(['message' => 'Data not availabe'], 500);
        return response()->json($data, 200);
    }

    public function usersData($id)
    {
        $user = SubUser::find($id);
        return $user;
        if (is_null($user)) return response()->json(['message' => 'Data not availabe'], 502);
        return response()->json($user, 200);
    }
}
