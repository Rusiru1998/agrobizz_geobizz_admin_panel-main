<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/apiRegistration', function () {return view('auth/register');});
Route::get('/AdminPanel/admin', 'AdminController@adminHome');
Route::get('/AdminPanel/pageViewGroups', 'AdminController@adminHome');
Route::get('/AdminPanel/pageEditGroup', 'AdminController@adminHome');
Route::get('/AdminPanel/pageViewMembers', 'AdminController@adminHome');
Route::get('/AdminPanel/pageEditMember', 'AdminController@adminHome');
Route::get('/AdminPanel/pageViewRecords', 'AdminController@adminHome');
Route::get('/AdminPanel/AdminSettings', 'AdminController@adminHome');
Route::get('/AdminPanel/TransferOfOwnership', 'AdminController@adminHome');
Route::get('/AdminPanel/mailAccounts', 'AdminController@adminHome');
Route::get('/AdminPanel/sendEmail', 'AdminController@adminHome');
Route::get('/AdminPanel/ImportCSV', 'AdminController@adminHome');
Route::get('/AdminPanel/Backups', 'AdminController@adminHome');
Route::get('/AdminPanel/FileBackups', 'AdminController@adminHome');
Route::get('/AdminPanel/FullBackups', 'AdminController@adminHome');
Route::get('/AdminPanel/DatabaseBackups', 'AdminController@adminHome');
Route::get('/AdminPanel/backforEmails', 'AdminController@adminHome');
Route::get('/AdminPanel/restore', 'AdminController@adminHome');
Route::get('/AdminPanel/errors', 'AdminController@adminHome');
Route::get('/AdminPanel/IPBlocker.vue', 'AdminController@adminHome');
Route::get('/AdminPanel/manageAPITokens.vue', 'AdminController@adminHome');
Route::get('/AdminPanel/noticeManagement', 'AdminController@adminHome');
Route::get('/AdminPanel/userLoginManagement.vue', 'AdminController@adminHome');
Route::get('/AdminPanel/siteInformation', 'AdminController@adminHome');

Route::get('AdminPanel/allUserInformationView10', 'AdminController@adminHome');
