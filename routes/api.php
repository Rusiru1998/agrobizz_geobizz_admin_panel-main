<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| Registration Routes
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|
| Here is where you put register API routes for Agrobizz.
| Most of the API targeted to Mobile app and also Agrobizz buyer panel
| And Agrobizz admin panel on web using the registration API
|
*/
//farmer Register
Route::post('/farmer/register', [
    'uses' => 'RegistrationAPIController@farmer'
]);
//create  farm
Route::post('/farm/create', [
    'uses' => 'RegistrationAPIController@farm'
]);
//update farm
Route::post('/farm/update', [
    'uses' => 'RegistrationAPIController@farm'
]);
//create crop
Route::post('/crop/create', [
    'uses' => 'RegistrationAPIController@crop'
]);
//update crop
Route::post('/crop/update', [
    'uses' => 'RegistrationAPIController@crop'
]);
//create process products
Route::post('/processProducts/create', [
    'uses' => 'RegistrationAPIController@processProducts'
]);
//update process products
Route::post('/processProducts/update', [
    'uses' => 'RegistrationAPIController@processProducts'
]);
//create  transporter
Route::post('/transporter/create', [
    'uses' => 'RegistrationAPIController@transporter'
]);
//update transporter
Route::post('/transporter/update', [
    'uses' => 'RegistrationAPIController@transporter'
]);
//delete transporter
Route::post('/transporter/delete', [
    'uses' => 'RegistrationAPIController@deleteTransporter'
]);
//create  customer
Route::post('/customer/create', [
    'uses' => 'RegistrationAPIController@customer'
]);
//update customer
Route::post('/customer/update', [
    'uses' => 'RegistrationAPIController@customer'
]);
//delete customer
Route::post('/customer/delete', [
    'uses' => 'RegistrationAPIController@deleteCustomer'
]);
//create  processor
Route::post('/processor/create', [
    'uses' => 'RegistrationAPIController@processor'
]);
//update processor
Route::post('/processor/update', [
    'uses' => 'RegistrationAPIController@processor'
]);
//delete processor
Route::post('/processor/delete', [
    'uses' => 'RegistrationAPIController@deleteProcessor'
]);
//create  retailer
Route::post('/retailer/create', [
    'uses' => 'RegistrationAPIController@retailer'
]);
//update retailer
Route::post('/retailer/update', [
    'uses' => 'RegistrationAPIController@retailer'
]);
//delete retailer
Route::post('/retailer/delete', [
    'uses' => 'RegistrationAPIController@deleteRetailer'
]);
//create  collector
Route::post('/collector/create', [
    'uses' => 'RegistrationAPIController@collector'
]);
//update collector
Route::post('/collector/update', [
    'uses' => 'RegistrationAPIController@collector'
]);
//delete collector
Route::post('/collector/delete', [
    'uses' => 'RegistrationAPIController@deleteCollector'
]);
//create  agent
Route::post('/agent/create', [
    'uses' => 'RegistrationAPIController@agent'
]);
//update agent
Route::post('/agent/update', [
    'uses' => 'RegistrationAPIController@agent'
]);
//delete agent
Route::post('/agent/delete', [
    'uses' => 'RegistrationAPIController@deleteAgent'
]);

//////////////////////////////////END OF REGISTER API\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

















/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| GET BY Agent Routes
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|
| Here is where you put API that data request from Agent.
| Most of the API targeted to Mobile app.
|
*/
//farmer Register
Route::post('/agent/getfarmerList', [
    'uses' => 'MobileAPIController@getFarmerByAgent'
]);
//get farmer by ID
Route::post('/agent/getFarmerByID', [
    'uses' => 'MobileAPIController@getFarmerByID'
]);
//get crop data by agent
Route::post('/crop/getData/ByAgent', [
    'uses' => 'MobileAPIController@getCropsByAgent'
]);
//get fertilizer data by agent
Route::post('/fertilizer/getData/ByAgent', [
    'uses' => 'MobileAPIController@getFertilizerByAgent'
]);
//get farm data by agent
Route::post('/farms/getData/ByAgent', [
    'uses' => 'MobileAPIController@getFarmByAgent'
]);
//get bank data by agent
Route::post('/bank/getData/ByAgent', [
    'uses' => 'MobileAPIController@getBankByAgent'
]);
//get products by agent
Route::post('/products/getData/ByAgent', [
    'uses' => 'MobileAPIController@getProcessProductsByAgent'
]);
//get analysis by agent
Route::post('/analysis/getData/ByAgent', [
    'uses' => 'MobileAPIController@getAnalysisByAgent'
]);
//get transporter by agent
Route::post('/transporter/getByAgent', [
    'uses' => 'MobileAPIController@getTransporterByAgent'
]);
// get processor by agent
Route::post('/processor/getByAgent', [
    'uses' => 'MobileAPIController@getProcessorByAgent'
]);
//get retailer by agent
Route::post('/retailer/getByAgent', [
    'uses' => 'MobileAPIController@getRetailerByAgent'
]);
//search collector by agent
Route::post('/collector/getByAgent', [
    'uses' => 'MobileAPIController@getCollectorByAgent'
]);
//agent search by agent
Route::post('/agent/getByAgent', [
    'uses' => 'MobileAPIController@getAgentByAgent'
]);
//get registered data by agent
Route::post('/registrations/getData/ByAgent', [
    'uses' => 'MobileAPIController@getRegistrationDataByAgent'
]);
//////////////////////////////////END OF GET By Agent API\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\















//================Authentication service=============================
//---------validate data by checking email or password exist-----------------------
Route::post('validateData', [
    'uses' => 'Auth\AuthController@validateData'
]);
//-------------------------check user login is invalid ----------------------------
Route::post('checkLogin', [
    'uses' => 'Auth\AuthController@checkLogin'
]);


//================Main API=============================
//------------------------select districts------------------------
Route::post('user/selectDistricts', [
    'uses' => 'APIController@addSelectedDistrict'
]);
//-------------------------get registered farmers----------------------------
Route::post('agriculture/getFarmers', [
    'uses' => 'APIController@getFarmers'
]);

//**************************************************REGISTRATION*******************************************
//-------------------------register farmer ----------------------------
Route::post('agriculture/bottomPanel/registration/farmerRegister', [
    'uses' => 'APIController@farmerRegister'
]);
//-------------------------register farm----------------------------
Route::post('agriculture/bottomPanel/registration/farmRegister', [
    'uses' => 'APIController@farmRegister'
]);
//-------------------------add crops ----------------------------
Route::post('agriculture/bottomPanel/registration/addCrop', [
    'uses' => 'APIController@addCrop'
]);


//*****************************************************************************ANALYZE*********************************************
//-------------------------get boundary details for crops----------------------------
Route::post('agriculture/bottomPanel/analysis/crops/getLocationDataFromFarmer', [
    'uses' => 'APIController@getLocationDataFromFarmer'
]);

//-------------------------get boundary details for fertilizer----------------------------
Route::post('agriculture/bottomPanel/analysis/fertilizer/getLocationDataFromFertilizer', [
    'uses' => 'APIController@getLocationDataFromFertilizer'
]);

//-------------------------Do proximity analysis----------------------------
Route::post('agriculture/bottomPanel/analysis/proximityAnalysis', [
    'uses' => 'APIController@proximityAnalysis'
]);

//-------------------------weather API----------------------------
Route::post('agriculture/bottomPanel/analysis/weatherAPI', [
    'uses' => 'APIController@weatherAPI'
]);

//-------------------------retrieve custom overlay----------------------------
Route::post('agriculture/bottomPanel/analysis/getCustomLayers', [
    'uses' => 'APIController@getCustomLayers'
]);
//-------------------------upload custom overlay----------------------------
Route::post('agriculture/bottomPanel/analysis/uploadCustomImage', [
    'uses' => 'APIController@uploadCustomImage'
]);

//-------------------------add daily collection----------------------------
Route::post('agriculture/bottomPanel/dailyCollection/addData', [
    'uses' => 'APIController@createDailyCollection'
]);

//-------------------------Fertilizer register----------------------------
Route::post('agriculture/bottomPanel/fertilizer/register', [
    'uses' => 'APIController@fertilizer'
]);
//-------------------------Tracking Device  register----------------------------
Route::post('agriculture/rightPanel/agent/registration', [
    'uses' => 'APIController@agentRegister'
]);



//*****************************************************************************Layers*********************************************
//-------------------------save custom points----------------------------
Route::post('agriculture/mapDataLayers/points/save', [
    'uses' => 'APIController@saveCustomMarkers'
]);
//-------------------------get custom points----------------------------
Route::post('agriculture/mapDataLayers/points/get', [
    'uses' => 'APIController@getCustomMarkers'
]);
//-------------------------save geojson layer----------------------------
Route::post('agriculture/mapDataLayers/geojson/save', [
    'uses' => 'APIController@saveGeoJson'
]);
//-------------------------get saved geojson layers----------------------------
Route::post('agriculture/mapDataLayers/geojson/get', [
    'uses' => 'APIController@getGeoJsonLayers'
]);
//-------------------------GIS  Layers By Boundary types----------------------------
Route::post('agriculture/rightPanel/gisLayers/byBoundaryTypes', [
    'uses' => 'GISLayerController@getGISLayerByBoundaryType'
]);
//-------------------------GIS  Layers for analysis----------------------------
Route::post('agriculture/bottomPanel/SuitabilityAnalysis/getMainDataset', [
    'uses' => 'GISLayerController@getAnalysisData'
]);
//-------------------------GIS  Layers Data----------------------------
Route::post('agriculture/rightPanel/gisLayers/dataset', [
    'uses' => 'GISLayerController@getGISLayerData'
]);

//-------------------------GND  Data Request----------------------------
Route::post('agriculture/gisLayers/dataset', [
    'uses' => 'APIController@getGNDData'
]);



//*****************************************************************************Data Upload*********************************************
//-------------------------get map icons----------------------------
Route::post('agriculture/leftPanel/addData/getMapIcons', [
    'uses' => 'APIController@getMapIcons'
]);

//------------------------shape file upload---------------------------
Route::post('agriculture/leftPanel/addData/uploadShapeFile', [
    'uses' => 'FileUploadController@shapeFileUpload'
]);
//------------------------shape file upload---------------------------
Route::post('agriculture/leftPanel/addData/uploadKML', [
    'uses' => 'FileUploadController@uploadKML'
]);
//-------------------------------Agent-----------------------------
Route::post('/agriculture/rightPanel/agent/getAgentLocation', [
    'uses' => 'APIController@getAgentLastLocation'
]);

//-------------------------------Remove app hint-----------------------------
Route::post('/user/hideHint', [
    'uses' => 'APIController@hideAppHint'
]);
Route::post('/agriculture/rightPanel/analysis/proximityAnalysis', [
    'uses' => 'GISLayerController@getGISProximityData'
]);

//*********************send notifications********************************************************
Route::post('/agriculture/rightPanel/BoundaryQuery/locationData', [
    'uses' => 'GISLayerController@getLocationDataFromBoundary'
]);
//*********************Demographic********************************************************
Route::post('/agriculture/demographic/syncData', [
    'uses' => 'GISLayerController@syncDemographic'
]);

//********************Crop damage*******************************************************
Route::post('/agriculture/crop/damages/get', [
    'uses' => 'APIController@getCropDamages'
]);

//-------------------------get all crop data----------------------------
Route::post('agriculture/crop/getAllCrops', [
    'uses' => 'APIController@getAllCrops'
]);
//-------------------------get all crop data----------------------------
Route::post('agriculture/bottomPanel/getAgents', [
    'uses' => 'APIController@getAgents'
]);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/**************************************************************************************************************
 * ----------------------------------MOBILE ROUTES-------------------------------------------------------------
 *------------------- Calling to MobileAPIController_OLD-----------------------------------------------------------
 * ------------------Designed for communicate with mobile application (Responsible Chamath)--------------------
 * ***********************************************************************************************************
 */






//User LOGIN
Route::post('/user/login', [
    'uses' => 'MobileAPIController@login'
]);

//create analyse
Route::post('/analyse/create', [
    'uses' => 'MobileAPIController@analysis'
]);
//update analyse
Route::post('/analyse/update', [
    'uses' => 'MobileAPIController@analysis'
]);

//create analyse
Route::post('/bankAccount/create', [
    'uses' => 'MobileAPIController@bankDetails'
]);
//update analyse
Route::post('/bankAccount/update', [
    'uses' => 'MobileAPIController@bankDetails'
]);


//create fertilizer
Route::post('/fertilizer/create', [
    'uses' => 'MobileAPIController@fertilizer'
]);
//update fertilizer
Route::post('/fertilizer/update', [
    'uses' => 'MobileAPIController@fertilizer'
]);



//crop damages
Route::post('/crop/damages/cud', [
    'uses' => 'MobileAPIController@cropDamage'
]);
//crop damages get
Route::post('/crop/damages/getByFarmer', [
    'uses' => 'MobileAPIController@getCropDamageByFarmer'
]);

//-----------------------get  crop data-------------------------------

Route::post('/crop/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getCropsByFarmer'
]);
Route::post('/crop/getData/ByFarm', [
    'uses' => 'MobileAPIController@getCropsByFarm'
]);
Route::post('/crop/getData/ByID', [
    'uses' => 'MobileAPIController@getCropsByID'
]);

//-----------------------get  Farm data-------------------------------

Route::post('/farms/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getFarmByFarmer'
]);
Route::post('/farms/getData/ByFarm', [
    'uses' => 'MobileAPIController@getFarmByFarm'
]);

//-----------------------get  Fertilizer data-------------------------------

Route::post('/fertilizer/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getFertilizerByFarmer'
]);
Route::post('/fertilizer/getData/ByID', [
    'uses' => 'MobileAPIController@getFertilizerByID'
]);

//-----------------------get  Bank data-------------------------------

Route::post('/bank/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getBankByFarmer'
]);
Route::post('/bank/getData/ByID', [
    'uses' => 'MobileAPIController@getBankByID'
]);



//-----------------------get  Products data-------------------------------

Route::post('/products/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getProcessProductsByFarmer'
]);
Route::post('/products/getData/ByID', [
    'uses' => 'MobileAPIController@getProcessProductsByID'
]);


//-----------------------get  Analysis data-------------------------------
Route::post('/analysis/getData/ByFarmer', [
    'uses' => 'MobileAPIController@getAnalysisByFarmer'
]);
Route::post('/analysis/getData/ByID', [
    'uses' => 'MobileAPIController@getAnalysisByID'
]);

//----------------------farmer search------------------------------
Route::post('/farmers/search', [
    'uses' => 'MobileAPIController@farmerSearchByName'
]);

Route::post('/agriculture/getFarmerDetails', [
    'uses' => 'ProfileController@getFarmerDetails'
]);

Route::post('/agriculture/getAgentDetails', [
    'uses' => 'ProfileController@getAgentDetails'
]);

Route::post('/agriculture/getCollectorDetails', [
    'uses' => 'ProfileController@getCollectorDetails'
]);

Route::post('/agriculture/getRetailerDetails', [
    'uses' => 'ProfileController@getRetailerDetails'
]);

Route::post('/agriculture/getProcessorDetails', [
    'uses' => 'ProfileController@getProcessorDetails'
]);

Route::post('/agriculture/getCustomerDetails', [
    'uses' => 'ProfileController@getCustomerDetails'
]);

Route::post('/agriculture/getTransporterDetails', [
    'uses' => 'ProfileController@getTransporterDetails'
]);

Route::get('/agriculture/gotoTrading', [
    'uses' => 'AdminController@tradingHome'
]);


//=========================================TRADING WEB API COLLECTION (Admin panel T U J)=======================================================
//get harvesting dataset
Route::post('/agriculture/admin/harvesting/get', [
    'uses' => 'AdminController@getHarvesting'
]);
//get live admin dataset
Route::post('/agriculture/admin/livetrading/get', [
    'uses' => 'AdminController@getLiveTrading'
]);
//get farmer confirmed dataset
Route::post('/agriculture/admin/confirmed/get', [
    'uses' => 'AdminController@getConfirmedTrading'
]);
//get farmer rejected dataset
Route::post('/agriculture/admin/confirmed/reject', [
    'uses' => 'AdminController@getFarmerRejectedTrading'
]);
//assign transporter
Route::post('/agriculture/admin/transporter/assign', [
    'uses' => 'AdminController@deliveryStarted'
]);
//get transport data
Route::post('/agriculture/admin/delivery/get', [
    'uses' => 'AdminController@getDelivery'
]);
//get delivered data
Route::post('/agriculture/admin/delivery/completed', [
    'uses' => 'AdminController@getDeliveredOrders'
]);

//Crop management
Route::post('/agriculture/admin/crops/CUD', [
    'uses' => 'AdminController@cropManage'
]);

//Agent list
Route::post('/agriculture/admin/users/agents', [
    'uses' => 'AdminController@getAgents'
]);

//Edit Agent
Route::post('/agriculture/admin/users/editAgents', [
    'uses' => 'ProfileController@editAgents'
]);

//Edit Farmer
Route::post('/agriculture/admin/users/editFarmer', [
    'uses' => 'ProfileController@editFarmer'
]);

//Collector list
Route::post('/agriculture/admin/users/collectors', [
    'uses' => 'AdminController@getCollectors'
]);

//Edit Collector
Route::post('/agriculture/admin/users/editCollector', [
    'uses' => 'ProfileController@editCollector'
]);

//Retailer list
Route::post('/agriculture/admin/users/retailers', [
    'uses' => 'AdminController@getRetailers'
]);

//Edit Retailer
Route::post('/agriculture/admin/users/editRetailer', [
    'uses' => 'ProfileController@editRetailer'
]);

//Processor list
Route::post('/agriculture/admin/users/processors', [
    'uses' => 'AdminController@getProcessors'
]);

//Edit Processor
Route::post('/agriculture/admin/users/editProcessor', [
    'uses' => 'ProfileController@editProcessor'
]);

//Customer list
Route::post('/agriculture/admin/users/customers', [
    'uses' => 'AdminController@getCustomers'
]);

//Edit Customer
Route::post('/agriculture/admin/users/editCustomer', [
    'uses' => 'ProfileController@editCustomer'
]);

//Transporter list
Route::post('/agriculture/admin/users/transporters', [
    'uses' => 'AdminController@getTransporters'
]);
//Edit Transporter
Route::post('/agriculture/admin/users/editTransporter', [
    'uses' => 'ProfileController@editTransporter'
]);
//All list
Route::post('/agriculture/admin/users/all', [
    'uses' => 'AdminController@getAllUsers'
]);


/**************************************************************************************************************
 * ----------------------------------MOBILE ROUTES WRITE BY [PAVI PRABU]-------------------------------------------------------------
 *------------------- Calling to MobileAPIController -----------------------------------------------------------
 * ------------------Designed for communicate with mobile application (Responsible Chamath)--------------------
 * ***********************************************************************************************************
 */



//search by name transporter
Route::post('/transporter/getByName', [
    'uses' => 'MobileAPIController@getTransporterByName'
]);
//search by id transporter
Route::post('/transporter/getById', [
    'uses' => 'MobileAPIController@getTransporterById'
]);


// ********************************************************

//search by agent customer
Route::post('/customer/getByAgent', [
    'uses' => 'MobileAPIController@getCustomerByAgent'
]);
//search by name customer
Route::post('/customer/getByName', [
    'uses' => 'MobileAPIController@getCustomerByName'
]);
//search by id customer
Route::post('/customer/getById', [
    'uses' => 'MobileAPIController@getCustomerById'
]);



// ********************************************************


//search by name processor
Route::post('/processor/getByName', [
    'uses' => 'MobileAPIController@getProcessorByName'
]);
//search by id processor
Route::post('/processor/getById', [
    'uses' => 'MobileAPIController@getProcessorById'
]);


// ********************************************************


//search by name retailer
Route::post('/retailer/getByName', [
    'uses' => 'MobileAPIController@getRetailerByName'
]);
//search by id retailer
Route::post('/retailer/getById', [
    'uses' => 'MobileAPIController@getRetailerById'
]);


// ********************************************************


//search by name collector
Route::post('/collector/getByName', [
    'uses' => 'MobileAPIController@getCollectorByName'
]);
//search by id collector
Route::post('/collector/getById', [
    'uses' => 'MobileAPIController@getCollectorById'
]);


// ********************************************************


//search by name agent
Route::post('/agent/getByName', [
    'uses' => 'MobileAPIController@getAgentByName'
]);
//search by id agent
Route::post('/agent/getById', [
    'uses' => 'MobileAPIController@getAgentById'
]);

//get dsd data regarding district
Route::post('/dsd/getByDistrict', [
    'uses' => 'MobileAPIController@getDSDData'
]);
//get all dsd data
Route::post('/dsd/getAll', [
    'uses' => 'MobileAPIController@getAllDSDData'
]);
//get   crop translations
Route::post('/crops/getTranslation', [
    'uses' => 'MobileAPIController@getCropTranslation'
]);
//get all  crops
Route::post('/crops/getAll', [
    'uses' => 'MobileAPIController@getAllCrops'
]);
//get all Major crops
Route::post('/majorCrop/getAll', [
    'uses' => 'MobileAPIController@getMajorCrop'
]);
//get all Main crops--------------------------Updated 20.04.2021
Route::post('/mainCrop/getAll', [
    'uses' => 'MobileAPIController@getMainCrop'
]);
//get all Main crops-------------------------Updated 20.04.2021
Route::post('/mainCrop/getByMajorCrop', [
    'uses' => 'MobileAPIController@getMainCropByMajor'
]);
//get Sub crops by Main crop--------------------updated 20.04.2021
Route::post('/subCrop/getByMainCrop', [
    'uses' => 'MobileAPIController@getSubCropAccorMainCrop'
]);
//get All Sub crops ----------------------------
Route::post('/subCrop/getAll', [
    'uses' => 'MobileAPIController@getSubCrop'
]);
//get crop ID by Name
Route::post('/crops/getCropCategoryIDByName', [
    'uses' => 'MobileAPIController@getCropCategoryIDByName'
]);
//get translation By Name
Route::post('/crops/getCropTranslationByName', [
    'uses' => 'MobileAPIController@getCropTranslationByName'
]);


//=========================================Analysis Controller (T U J)==================================================
Route::post('/agriculture/analysis/getMainData', [
    'uses' => 'AnalysisController@getMainData'
]);
Route::post('/agriculture/analysis/getFertilizer', [
    'uses' => 'AnalysisController@getMainData'
]);

//=========================================Analysis Controller (Pavi Prabu)==================================================
Route::post('/agriculture/analysis/getNextMonthHarvesting', [
    'uses' => 'AnalysisController@getNextMonthHarvesting'
]);
Route::post('/agriculture/analysis/getLiveTradingMajor', [
    'uses' => 'AnalysisController@getLiveTradingMajor'
]);
Route::post('/agriculture/analysis/getDailySales', [
    'uses' => 'AnalysisController@getDailySales'
]);
Route::post('/agriculture/analysis/getDistrictLevelUsers', [
    'uses' => 'AnalysisController@getDistrictLevelUsers'
]);

Route::post('/agriculture/analysis/getUserCount', [
    'uses' => 'AnalysisController@getCountofUsers'
]);


/*
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| TRADING MOBILE ROUTES
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|
| Mobile API routes for admin
| Most of the API targeted to Mobile app.
|
*/
//create harvest
Route::post('/agroTrading/harvest/create', [
    'uses' => 'TradingMobileAPIController@addToHarvesting'
]);
//get harvest by farmer
Route::post('/agroTrading/harvest/getByFarmer', [
    'uses' => 'TradingMobileAPIController@getHarvestingBYFarmer'
]);
//get harvesting By ID
Route::post('/agroTrading/harvest/getByID', [
    'uses' => 'TradingMobileAPIController@getHarvestingBYID'
]);
//get harvest as grouped data
Route::post('/agroTrading/harvest/getAsGrouped', [
    'uses' => 'TradingMobileAPIController@getHarvestingGrouped'
]);
//create new order
Route::post('/agroTrading/liveTrading/addNew', [
    'uses' => 'TradingMobileAPIController@addToLiveTrading'
]);
//confirm the order
Route::post('/agroTrading/liveTrading/farmerResponse', [
    'uses' => 'TradingMobileAPIController@confirmLiveTradeByFarmer'
]);
//get firebase data
Route::post('/agroTrading/getfirebaseData', [
    'uses' => 'TradingMobileAPIController@delivery_started'
]);
//get firebase data
Route::post('/agroTrading/liveTrading/tradingsByFarmer', [
    'uses' => 'TradingMobileAPIController@getTradingsByFarmer'
]);
//get admin By ID
Route::post('/agroTrading/liveTrading/tradingsByID', [
    'uses' => 'TradingMobileAPIController@getTradingByID'
]);
//get admin By ID
Route::post('/agroTrading/delivery/getOrdersByTransporter', [
    'uses' => 'TradingMobileAPIController@getOrdersByTransporter'
]);
//accepted by transporter
Route::post('/agroTrading/delivery/TransporterResponseByInvoice', [
    'uses' => 'TradingMobileAPIController@TransporterResponseByInvoice'
]);
//rejected by transporter
Route::post('/agroTrading/delivery/TransporterResponseByTrading', [
    'uses' => 'TradingMobileAPIController@TransporterResponseByTrading'
]);
//rejected by transporter
Route::post('/agroTrading/delivery/getByInvoiceNo', [
    'uses' => 'TradingMobileAPIController@getDeliveryByInvoiceNo'
]);
//get nearest point by farmer
Route::post('/agroTrading/delivery/getNearestPointByLatLng', [
    'uses' => 'TradingMobileAPIController@nearestPickLocationByLatLng'
]);

//invoice picked by farmer
Route::post('/agroTrading/delivery/invoicePicked', [
    'uses' => 'TradingMobileAPIController@invoicePicked'
]);
//invoice delivered by farmer
Route::post('/agroTrading/delivery/invoiceDelivered', [
    'uses' => 'TradingMobileAPIController@invoiceDelivered'
]);
//get drivers next location
Route::post('/agroTrading/delivery/getNextLocation', [
    'uses' => 'TradingMobileAPIController@driversNextLocation'
]);
//set live tracking request
Route::post('/agroTrading/delivery/requestLiveLocation', [
    'uses' => 'TradingMobileAPIController@requestLiveLocation'
]);

Route::post('/agroTrading/delivery/calculateTotalPrice', [
    'uses' => 'TradingMobileAPIController@calTransportPrice'
]);

Route::post('/agroTrading/payment/tradingPaymentByInvoice', [
    'uses' => 'TradingMobileAPIController@tradingPaymentByInvoice'
]);

//////////////////////////////////END OF TRADING MOBILE API\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\




//=========================================TRADING BUYER API COLLECTION (Pavithra)=======================================================
//get Major Crop with image
Route::post('/agroTrading/majorCrop/get', [
    'uses' => 'AdminController@getMajorCropCategory'
]);

//get Main Crop with image
Route::post('/agroTrading/mainCrop/get', [
    'uses' => 'AdminController@getCropCategory'
]);

//get Sub Crop by Main Crop
Route::post('/agroTrading/subCropByMainCrop/get', [
    'uses' => 'AdminController@getSubCropByMainCrop'
]);

//get Farmer details by Sub Crop
Route::post('/agroTrading/farmerDetailsBySubCrop/get', [
    'uses' => 'AdminController@getFarmerDetailsBySubCrop'
]);

//get admin By User ID
Route::post('/agroTrading/liveTrading/getInvoiceByUserId', [
    'uses' => 'AdminController@getInvoiceByUserId'
]);
//delete User
Route::post('/agriculture/deleteUser', [
    'uses' => 'ProfileController@deleteUser'
]);


//=========================================TRADING MOBILe BUYER API COLLECTION for Chamath (Dev Pavithra)=======================================================

//get Major Crop with image
Route::post('/agroTrading/mobile/majorCrop/get', [
    'uses' => 'TradingMobileAPIController@getMajorCropCategory'
]);

//get Main Crop with image
Route::post('/agroTrading/mobile/mainCrop/get', [
    'uses' => 'TradingMobileAPIController@getCropCategory'
]);

//get Sub Crop by Main Crop
Route::post('/agroTrading/mobile/subCropByMainCrop/get', [
    'uses' => 'TradingMobileAPIController@getSubCropByMainCrop'
]);

//FCM token update
Route::post('/tokens/fcm', [
    'uses' => 'MobileAPIController@updateFCMToken'
]);

Route::post('/getAgentTradingData', [
    'uses' => 'MobileAPIController@getAgentTradingData'
]);



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//dashboard routes

Route::get('web-users', [
    'uses' => 'AdminDashboard@getWebUsers'
]);

Route::get('mobile-users', [
    'uses' => 'AdminDashboard@getMobileUsers'
]);


Route::get('block-users', [
    'uses' => 'AdminDashboard@getBlockUsers'
]);


Route::get('admin', [
    'uses' => 'AdminDashboard@getAdmin'
]);

Route::get('active-users', [
    'uses' => 'AdminDashboard@getActiveUsers'
]);


Route::get('black-list-users', [
    'uses' => 'AdminDashboard@getBlackListUsers'
]);

Route::get('all-users', [
    'uses' => 'AdminDashboard@allUsers'
]);

Route::post('add-user-data', [
    'uses' => 'AdminDashboard@addUsers'
]);


Route::get('users-data/{id}', [
    'uses' => 'AdminDashboard@usersData'
]);
